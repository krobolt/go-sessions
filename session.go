package session

import (
	"net/http"

	"github.com/gorilla/Sessions"
)

//Session mathods
type Session interface {
	Has(key string) bool
	Set(key string, value interface{}) bool
	SetMultiple(values map[string]interface{}) bool
	Get(key string) (interface{}, bool)
	GetMultiple(keys []string) (map[string]interface{}, bool)
}

type session struct {
	session *sessions.Session
	res     http.ResponseWriter
	req     *http.Request
}

//GetSession returns a session for that request
func GetSession(c *sessions.CookieStore, name string, r *http.Request, w http.ResponseWriter) (Session, error) {
	s, err := c.Get(r, name)
	if err != nil {
		return nil, err
	}
	return &session{
		session: s,
		req:     r,
		res:     w,
	}, nil
}

//CreateCookieStore create cookie store
func CreateCookieStore(secret string) *sessions.CookieStore {
	return sessions.NewCookieStore([]byte(secret))
}

//Has check if session value is present
func (s session) Has(key string) bool {
	if _, ok := s.session.Values[key]; ok {
		return true
	}
	return false
}

//Set a session key value
func (s session) Set(key string, value interface{}) bool {
	s.session.Values[key] = value
	s.session.Save(s.req, s.res)
	if !s.Has(key) {
		return false
	}
	return true
}

//SetMultiple multiple session key values
func (s session) SetMultiple(values map[string]interface{}) bool {
	saved := true
	for key, value := range values {
		ok := s.Set(key, value)
		if !ok {
			saved = false
		}
	}
	return saved
}

//Get return session key value
func (s session) Get(key string) (interface{}, bool) {
	if s.Has(key) {
		return s.session.Values[key], true
	}
	return nil, false
}

//GetMultiple get multiple key values and return as map
func (s session) GetMultiple(keys []string) (map[string]interface{}, bool) {
	out := make(map[string]interface{})
	ok := true
	for _, value := range keys {
		if s.Has(value) {
			out[value] = s.session.Values[value]
		} else {
			ok = false
		}
	}
	return out, ok
}
