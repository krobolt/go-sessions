package session

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"testing"

	sessions "github.com/gorilla/Sessions"
	"github.com/stretchr/testify/assert"
)

var (
	Cookies *sessions.CookieStore
)

//CreateSession Return session
func CreateSession() *Session {
	Cookies := sessions.NewCookieStore([]byte("mysuperdupersecret"))
	req, _ := http.NewRequest("GET", "http://localhost:8080/", nil)
	rsp := NewRecorder()
	session, _ := StartSession(Cookies, "myappcookies", req, rsp)
	return session
}

// NewRecorder returns an initialized ResponseRecorder.
func NewRecorder() *httptest.ResponseRecorder {
	return &httptest.ResponseRecorder{
		HeaderMap: make(http.Header),
		Body:      new(bytes.Buffer),
	}
}

func TestStart(t *testing.T) {
	Cookies := sessions.NewCookieStore([]byte("mysuperdupersecret"))
	req, _ := http.NewRequest("GET", "http://localhost:8080/", nil)
	rsp := NewRecorder()
	session, _ := StartSession(Cookies, "myappcookies", req, rsp)
	expected := &Session{
		session.session, rsp, req,
	}
	assert.Equal(t, session, expected)
}

func TestHas(t *testing.T) {
	session := CreateSession()
	session.session.Values["keyAlt"] = "someValue"
	if !session.Has("keyAlt") {
		t.Error("missing session value")
	}
}

func TestSet(t *testing.T) {
	session := CreateSession()
	session.Set("key", "value")
	session2, _ := StartSession(Cookies, "myappcookies", session.req, session.res)
	if !session2.Has("key") {
		t.Error("missing session value")
	}
}

func TestSetMultiple(t *testing.T) {
	session := CreateSession()

	keyPairs := make(map[string]interface{})
	keyPairs["keyA"] = "valueA"
	keyPairs["keyB"] = "valueB"
	keyPairs["keyC"] = "valueC"
	session.SetMultiple(keyPairs)
	session2, _ := StartSession(Cookies, "myappcookies", session.req, session.res)

	for key := range keyPairs {
		if !session2.Has(key) {
			t.Error("missing session value")
		}
		sv, _ := session2.Get(key)
		if sv != keyPairs[key] {
			t.Error("session value is not the same")
		}
	}
}

func TestGet(t *testing.T) {
	session := CreateSession()
	session.session.Values["getKey"] = "someValue"
	val, ok := session.Get("getKey")
	assert.Equal(t, "someValue", val)
	if !ok {
		t.Error("missing session value")
	}
}

func TestGetMissing(t *testing.T) {
	session := CreateSession()

	val, ok := session.Get("getKey")

	if ok {
		t.Error("ok should be false as value not found")
	}

	if val != nil {
		t.Error("Value should be nil")
	}

}

func TestGetMultiple(t *testing.T) {
	session := CreateSession()
	session.session.Values["key1"] = "value1"
	session.session.Values["key2"] = "value2"
	session.session.Values["key3"] = "value3"
	values, ok := session.GetMultiple([]string{"key1", "key2", "key3"})
	if !ok {
		t.Error("missing session value")
	}
	assert.Equal(t, "value1", values["key1"])
	assert.Equal(t, "value2", values["key2"])
	assert.Equal(t, "value3", values["key3"])
}

func TestGetMultipleMissing(t *testing.T) {
	session := CreateSession()
	session.session.Values["key1"] = "value1"
	session.session.Values["key2"] = "value2"

	values, ok := session.GetMultiple([]string{"key1", "key2", "key3"})
	if ok {
		t.Error("value missing, should cause error", ok)
	}
	assert.Equal(t, "value1", values["key1"])
	assert.Equal(t, "value2", values["key2"])
}
